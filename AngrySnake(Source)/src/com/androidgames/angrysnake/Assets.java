package com.androidgames.angrysnake;

import com.androidgames.framework.Music;

import com.androidgames.framework.Pixmap;
import com.androidgames.framework.Sound;
//����� ���������� ��� ����������� �����
public class Assets {
    public static Pixmap background;
    public static Pixmap logo;
    public static Pixmap mainMenu;
    public static Pixmap buttons;
    public static Pixmap history1;
    public static Pixmap history2;
    public static Pixmap history3;
    public static Pixmap numbers;
    public static Pixmap ready;
    public static Pixmap pause;
    public static Pixmap gameOver;
    public static Pixmap headUp;
    public static Pixmap headLeft;
    public static Pixmap headDown;
    public static Pixmap headRight;
    public static Pixmap tail;
    public static Pixmap egg1;
    public static Pixmap egg2;
    public static Pixmap egg3; 
    public static Pixmap level; 
    
    public static Sound click;
    public static Sound eat;
    public static Sound bitten;
    
    public static Music myMusic;
}
