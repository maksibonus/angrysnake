package com.androidgames.angrysnake;

import java.util.List;

import com.androidgames.framework.Game;
import com.androidgames.framework.Graphics;
import com.androidgames.framework.Input.TouchEvent;
import com.androidgames.framework.Screen;

public class ComplexityScreen extends Screen{
	public ComplexityScreen(Game game) {
        super(game);
    }

    @Override
    public void update(float deltaTime) {
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();
        
        int len = touchEvents.size();
        //�������� �� ���� ��������������
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x > 80 && event.x <= 240) {
                    if(event.y > 100 && event.y <= 148) {	
                        if(Settings.soundEnabled)
                        	//���� �������
                            Assets.click.play(1);
                        game.setScreen(new GameScreen(game,0.2f,false,"beatles.ogg"));
                        return;
                    }                    
                    if(event.y > 148 && event.y < 196) {
                        if(Settings.soundEnabled)
                            Assets.click.play(1);
                        game.setScreen(new GameScreen(game,0.1f,true,"monster.ogg"));                       
                        return;
                    }     
                }
                if(event.x < 64 && event.y > 416) {
                    if(Settings.soundEnabled)
                        Assets.click.play(1);
                    game.setScreen(new MainMenuScreen(game));
                    return;
                }
                //��������, ��� ������������� ��������� � ������� �������� ��������� �����
	            if(event.x > 257 && event.y < 64) {
	                Settings.soundEnabled = !Settings.soundEnabled;
	                if(Settings.soundEnabled)
	                    Assets.click.play(1);
	            }
            }
        }
    }

    @Override
    public void present(float deltaTime) {
        Graphics g = game.getGraphics();      
        g.drawPixmap(Assets.background, 0, 0);
        if(Settings.soundEnabled)
            g.drawPixmap(Assets.buttons, 256, 0, 0, 0, 64, 64);
        else
            g.drawPixmap(Assets.buttons, 256, 0, 64, 0, 64, 64);
        g.drawPixmap(Assets.level, 80, 100);
        g.drawPixmap(Assets.buttons, 0, 416, 64, 64, 64, 64);
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
    }
}
