package com.androidgames.angrysnake;

import java.util.Random;

public class World {
	//������� ����
    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 13;
    //��������� ��� �����
    static final int SCORE_INCREMENT = 10;
    //��������� ��������� ��������
    static final float TICK_INITIAL = 0.5f;
    //�� ������� �� ��������� ��������, ����� �������� 10 �����
    static final float TICK_DECREMENT = 0.05f;

    public Snake snake;
    public Egg egg;
    public boolean gameOver = false;
    public int score = 0;
    public float renderSpeed;

    boolean fields[][] = new boolean[WORLD_WIDTH][WORLD_HEIGHT];
    Random random = new Random();
    //������� �������
    float tickTime = 0;
    //���������� ����� ������� ������� ������ ����������� ��������� ����
    static float tick;

    public World(float speed) {
    	renderSpeed=speed;
        snake = new Snake();
        placeEgg();
        tick=TICK_INITIAL;
    }

    //���������� ������ ����
    private void placeEgg() {
        for (int x = 0; x < WORLD_WIDTH; x++) {
            for (int y = 0; y < WORLD_HEIGHT; y++) {
                fields[x][y] = false;
            }
        }
        //�������� ������� ������
        int len = snake.parts.size();
        for (int i = 0; i < len; i++) {
            SnakePart part = snake.parts.get(i);
            fields[part.x][part.y] = true;
        }
        //���� ��������� ������
        int stainX = random.nextInt(WORLD_WIDTH);
        int stainY = random.nextInt(WORLD_HEIGHT);
        while (true) {
            if (fields[stainX][stainY] == false)
                break;
            stainX += 1;
            if (stainX >= WORLD_WIDTH) {
                stainX = 0;
                stainY += 1;
                if (stainY >= WORLD_HEIGHT) {
                    stainY = 0;
                }
            }
        }
        egg = new Egg(stainX, stainY, random.nextInt(3));
    }

    public void update(float deltaTime) {
        if (gameOver)
        	return;         
        //������� ������ ������� �� ����������� �����(����� ���������� �������� � ���������� �����������)
        tickTime += deltaTime;
        while (tickTime > tick) {
            tickTime -= tick;
            snake.advance();
            if (snake.checkBitten()) {
                gameOver = true;
                return;
            }
            SnakePart head = snake.parts.get(0);
            if (head.x == egg.x && head.y == egg.y) {
                score += SCORE_INCREMENT;
                snake.eat();
                if (snake.parts.size() == WORLD_WIDTH * WORLD_HEIGHT) {
                    gameOver = true;
                    return;
                } else {
                    placeEgg();
                }
                //���� ����� 1(10) ����(���) � �������� ������� ������ renderSpeed
                if (score % (int)(Math.pow(10, renderSpeed*10))== 0 && tick - TICK_DECREMENT >= renderSpeed) {
                    tick -= TICK_DECREMENT;
                }
            }
        }
    }
}
