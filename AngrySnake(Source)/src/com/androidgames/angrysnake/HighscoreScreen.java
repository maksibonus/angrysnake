package com.androidgames.angrysnake;

import java.util.List;

import com.androidgames.framework.Game;
import com.androidgames.framework.Graphics;
import com.androidgames.framework.Screen;
import com.androidgames.framework.Input.TouchEvent;

public class HighscoreScreen extends Screen {
    String linesEasy[] = new String[5];
    String linesHard[] = new String[5];

    public HighscoreScreen(Game game) {
        super(game);
        for (int i = 0; i < 5; i++) {
            linesEasy[i] = "" + (i + 1) + ". " + Settings.highscoresEasy[i];
            linesHard[i] = "" + (i + 1) + ". " + Settings.highscoresHard[i];
        }
    }

    @Override
    public void update(float deltaTime) {
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();
        int len = touchEvents.size();
        for (int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if (event.type == TouchEvent.TOUCH_UP) {
                if (event.x < 64 && event.y > 416) {
                    if(Settings.soundEnabled)
                        Assets.click.play(1);
                    game.setScreen(new MainMenuScreen(game));
                    return;
                }
            }
        }
    }

    @Override
    public void present(float deltaTime) {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.background, 0, 0);
        g.drawPixmap(Assets.mainMenu, 64, 20, 0, 42, 196, 42);
        g.drawPixmap(Assets.level, 0, 80, 0, 0, 160, 48);
        g.drawPixmap(Assets.level, 160, 80, 0, 48, 160, 48);
        int y = 140;
        for (int i = 0; i < 5; i++) {
            drawText(g, linesEasy[i], 20, y);
            drawText(g, linesHard[i], 180, y);
            //�������� ����� ��������
            y += 50;
        }

        g.drawPixmap(Assets.buttons, 0, 416, 64, 64, 64, 64);
    }

    //������� ������ � ����� ��������
    public void drawText(Graphics g, String line, int x, int y) {
        int len = line.length();
        for (int i = 0; i < len; i++) {
        	//�������� ������ �������
            char character = line.charAt(i);
            if (character == ' ') {
                x += 20;
                continue;
            }
            int srcX = 0;
            int srcWidth = 0;
            if (character == '.') {
                srcX = 200;
                srcWidth = 10;
            } else {
            	//������������ �������� �� ��� � �� ����������� number.png
                srcX = (character - '0') * 20;
                srcWidth = 20;
            }
            g.drawPixmap(Assets.numbers, x, y, srcX, 0, srcWidth, 32);
            x += srcWidth;
        }
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
    }
}
