package com.androidgames.angrysnake;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import com.androidgames.framework.FileIO;

//���������� ���������������� ��������(����� ����� ������)
public class Settings {
	//������� �� ����
    public static boolean soundEnabled = true;
    public static int[] highscoresEasy = new int[] { 100, 80, 60, 40, 20 };
    public static int[] highscoresHard = new int[] { 100, 80, 60, 40, 20 };

    public static void load(FileIO files) {
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(
                    files.readFile(".angrysnake")));
            //������ ������
            soundEnabled = Boolean.parseBoolean(in.readLine());
            for (int i = 0; i < 5; i++) {
                highscoresEasy[i] = Integer.parseInt(in.readLine());
            }
            for (int i = 0; i < 5; i++) {
                highscoresHard[i] = Integer.parseInt(in.readLine());
            }
        } catch (IOException e) {
        } catch (NumberFormatException e) {
        } finally {
            try {
                if (in != null)
                    in.close();
            } catch (IOException e) {
            }
        }
    }

    public static void save(FileIO files) {
        BufferedWriter out = null;
        try {
            out = new BufferedWriter(new OutputStreamWriter(
                    files.writeFile(".angrysnake")));
            out.write(Boolean.toString(soundEnabled));
            out.write("\n");
            for (int i = 0; i < 5; i++) {
                out.write(Integer.toString(highscoresEasy[i])+"\n");
            }
            for (int i = 0; i < 5; i++) {
                out.write(Integer.toString(highscoresHard[i])+"\n");
            }

        } catch (IOException e) {
        } finally {
            try {
                if (out != null)
                    out.close();
            } catch (IOException e) {
            }
        }
    }
    
    //���������� ������ ���������� � ���-5 ��������� easy
    public static void addScoreEasy(int score) {
        for (int i = 0; i < 5; i++) {
            if (highscoresEasy[i] < score) {
                for (int j = 4; j > i; j--)
                	//�������� ���������� �������(������� ��������� ������)
                    highscoresEasy[j] = highscoresEasy[j - 1];
                //������ ��� ������ �� �����
                highscoresEasy[i] = score;
                break;
            }
        }
    }
    
    //���������� ������ ���������� � ���-5 ��������� hard
    public static void addScoreHard(int score) {
        for (int i = 0; i < 5; i++) {
            if (highscoresHard[i] < score) {
                for (int j = 4; j > i; j--)
                    highscoresHard[j] = highscoresHard[j - 1];
                highscoresHard[i] = score;
                break;
            }
        }
    }
}
